[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]



<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/jharrels/branches">
    <img src="https://j-topia.com/images/branches_logo.png" alt="Logo" width="100" height="100">
  </a>

  <h3 align="center">Branches</h3>

  <p align="center">
    A modern, clean tool for writing gamebooks and CYOA-style adventures with or without RPG elements.
    <br />
    <br />
    <a href="https://github.com/jharrels/branches/issues">Report Bug</a>
    ·
    <a href="https://github.com/jharrels/branches/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">What is Branches?</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## What is Branches?

[![Branches][braches-screenshot]](https://j-topia.com/images/branches_screenshot.png)

Branches is a well designed, clean, and modern writing tool for Gamebooks and CYOA-style adventures. It includes options for defining RPG elements (simple, basic, and advanced) as well as locations and items. Once you've created the perfect story, you have three choices:
* Export the story into a format that can be used to create e-reader books (Javascript-free).
* Export the story into HTML/Javascript/CSS, which can be played on any computer without Branches. Feel free to wrap it in Electron or use PhoneGap and get it out there!
* Upload the story to the Branches story archives so that others can easily play it either online or through a locally installed copy of Branches.

A list of resources that I find helpful are listed in the acknowledgements.

### Built With

* [Node.js](https://nodejs.org)
* [JQuery](https://jquery.com)
* [Electron](https://www.electronjs.org)
* [Font Awesome](https://fontawesome.com)

### We need help!
Currently Branches is being developed on a machine running Windows, but we purposely chose a cross-platform framework in which to develop it. We need help in the following areas:
* Mac-specific code and/or the ability to test on a Mac.
* Linux-specific code and/or the ability to test on Linux.
* Translations into other languages.

<!-- GETTING STARTED -->
## Getting Started

You've checked out Branches and would like to contribute? Great! Here's how to set up a working copy locally.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/jharrels/branches.git
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. Startup Branches from the repo
   ```sh
   npm test
   ```



<!-- ROADMAP -->
## Roadmap
* **0.1.0** - General project framework and layout.
* **0.2.0** - Ability to input general information: title, subtitle, author, description, RPG elements to use, etc.
* **0.3.0** - Segment editor implemented.
* **0.4.0** - Attributes editor implemented.
* **0.5.0** - Locations editor implemented.
* **0.6.0** - Items editor implemented.
* **0.7.0** - Exporting gamebooks for e-readers.
* **0.8.0** - Exporting gamebooks for distribution as HTML/JS/CSS.
* **0.9.0** - Exporting gamebooks to Branches story archives.
* **0.10.0** - Configuration options.
* **0.11.0** - Finalization.
* **0.12.0** - Build components (icons, extra files, etc).
* **1.0.0** - Release.

Planned features:
* Easy to visualize editor for creating gamebooks.
* Play gamebooks directly in the editor.
* RPG elements that come in three levels:
  * Simple (is Strength > 3)
  * Basic (Strength required is 3. You roll a single die and determine success. Simple combat.)
  * Advanced (Perform an action with average difficulty that requires strength. Roll several dice to determine success. Advanced combat.)
* Attributes - define attributes for role-playing (strength, dexterity, magic, etc).
* Locations - keep track of locations you know about, unlocking new choices.
* Items - keep track of items you hold, unlocking new choices.
* Export gamebooks in a way so that they can be easily converted into e-reader books (Kindle, etc). These gamebooks will contain no Javascript in order to maximize compatibility.
* Export gamebooks as HTML/Javascript/CSS. These files can be used to create a standalone game using Electron, NWJS, PhoneGap or other tools and then distributed.
* Upload gamebooks to the Branches Gamebook Archives to share with other users of Branches.

See the [open issues](https://github.com/jharrels/branches/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/amazing-feature`)
3. Commit your Changes (`git commit -m 'Add some amazing-feature'`)
4. Push to the Branch (`git push origin feature/amazing-feature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Project Link: [https://github.com/jharrels/branches](https://github.com/jharrels/branches)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Font Awesome](https://fontawesome.com)
* [Node.js](https://nodejs.org)
* [JQuery](https://jquery.com)
* [Electron](https://www.electronjs.org)
* [Choose an Open Source License](https://choosealicense.com)




<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/jharrels/branches.svg?style=for-the-badge
[contributors-url]: https://github.com/jharrels/branches/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/jharrels/branches.svg?style=for-the-badge
[forks-url]: https://github.com/jharrels/branches/network/members
[stars-shield]: https://img.shields.io/github/stars/jharrels/branches.svg?style=for-the-badge
[stars-url]: https://github.com/jharrels/branches/stargazers
[issues-shield]: https://img.shields.io/github/issues/jharrels/branches.svg?style=for-the-badge
[issues-url]: https://github.com/jharrels/branches/issues
[license-shield]: https://img.shields.io/github/license/jharrels/branches.svg?style=for-the-badge
[license-url]: https://github.com/jharrels/branches/blob/master/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/justin-harrelson-a7b18549
[branches-screenshot]: https://j-topia.com/images/branches_screenshot.png
