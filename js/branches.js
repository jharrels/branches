const { dialog } = require('electron').remote;
const path = require('path');
const os = require('os');
const fs = require('fs')
const { remote } = require('electron')
const app = remote.app
const { Menu, MenuItem } = remote
const electron = require('electron');
const { spawn } = require('child_process');
const ini = require('ini');

const electronScreen = require('electron').screen;
const Store = require('electron-store');
const store = new Store();
const customTitlebar = require('custom-electron-titlebar');

const template = [
   {
      label: 'Edit',
      submenu: [
         {
            role: 'undo'
         },
         {
            role: 'redo'
         },
         {
            type: 'separator'
         },
         {
            role: 'cut'
         },
         {
            role: 'copy'
         },
         {
            role: 'paste'
         }
      ]
   },

   {
      label: 'View',
      submenu: [
         {
            role: 'reload'
         },
         {
            role: 'toggledevtools'
         },
         {
            type: 'separator'
         },
         {
            role: 'resetzoom'
         },
         {
            role: 'zoomin'
         },
         {
            role: 'zoomout'
         },
         {
            type: 'separator'
         },
         {
            role: 'togglefullscreen'
         }
      ]
   },

   {
      role: 'window',
      submenu: [
         {
            role: 'minimize'
         },
         {
            role: 'close'
         }
      ]
   },

   {
      role: 'help',
      submenu: [
         {
            label: 'Learn More'
         }
      ]
   }
]

const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu);

let titlebar = new customTitlebar.Titlebar({
  backgroundColor: customTitlebar.Color.fromHex('#ddd'),
  menu: null,
  overflow: "hidden"
});

/* ----------------------------------------------------------------------------
   LOAD PREFS AND SETUP THE GUI AT LAUNCH
---------------------------------------------------------------------------- */
var prefs = loadPrefs();
repositionUI();

/* ----------------------------------------------------------------------------
   EVENT HANDLERS
---------------------------------------------------------------------------- */

$("#button-modal-folder-cancel").on("click", function() {
  hideModal("modal-folder");
});

$("#button-modal-folder-ok").on("click", function() {
  let parentNode = $treeObj.tree('getNodeById', selectedNodeId);
  let newName = $("#input-modal-folder-name").val();
  $treeObj.tree('appendNode', {name: '<i class="fas fa-folder"></i> '+newName, children: [], id: libraryMetadata['nextId']}, parentNode);
  let newNode = $treeObj.tree('getNodeById', libraryMetadata['nextId']);
  $treeObj.tree('selectNode', newNode);
  libraryMetadata['nextId']++;
  hideModal("modal-folder");
});

$("#divider-library").draggable({ containment: "parent", axis: "x",
    drag: function() {
        let position = $("#divider-library").position();
        let viewportWidth = window.outerWidth;
        let maxWidth = viewportWidth * .35;
        let minWidth = viewportWidth * .15;
        if ((position.left >= minWidth) && (position.left <= maxWidth)) {
          $("#pane-library").width(position.left + 5);
          $("#pane-editor").css({"left": position.left+6});
          let libraryWidth = $("#pane-library").width();
            $("#pane-editor").width(viewportWidth - libraryWidth)
            .css({"left": libraryWidth});
        }
    },
    stop: function() {
      $("#divider-library").css({"left":$("#pane-library").width()-5});
    }
});

/* ----------------------------------------------------------------------------
   UI FUNCTIONS
---------------------------------------------------------------------------- */
function repositionUI() {
  let viewportWidth = window.outerWidth;
  $("#divider-library").css({"left":$("#pane-library").width()-5});
  $("#divider-documents").css({"left":$("#pane-documents").width()+$("#pane-library").width()-5});
  if (os.type() == 'Darwin') {
  } else {
    $("#button-bar-editor").css({"left":viewportWidth - $(".window-controls-container").width() - $("#button-bar-editor").width()});
  }
}

function showModal(modalId) {
  $(`#${modalId}`).css({
    "display": "flex",
  });
}

function hideModal(modalId) {
  $(`#${modalId}`).css({
    "display": "none",
  });
}

/* ----------------------------------------------------------------------------
   PREFERENCE FUNCTIONS
---------------------------------------------------------------------------- */
function loadPrefs() {
  let prefs = store.get('prefs');
  if (prefs === undefined) prefs = {};
  return prefs;
}
